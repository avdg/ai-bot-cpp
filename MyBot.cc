#include "lib/stream.h"

struct MyBot : ants::Event {
	MyBot();
	void inline onConfig();
	void inline onRound();
	void inline onShutDown();
};

inline MyBot::MyBot(){};
void inline MyBot::onConfig(){}
void inline MyBot::onRound(){}
void inline MyBot::onShutDown(){}

int main(int argc, char *argv[]) {
	std::vector<ants::Event> events;
	events.push_back(MyBot());

	return ants::run(events);
}