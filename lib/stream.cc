#include <cassert>
#include <cstdlib>
#include <iostream>
#include <sstream>

#include "stream.h"
#include "logger.h"

Logger logger;

// ==> Stream.cc <==
// Reads stream and update state according to gathered information

// Coverage
void inline coverageRaisePos(Location pos) {
#ifdef VISUALISER
	std::cout << "v setFillColor 0 255 0 .5\n";
	std::cout << "v tile " << int(pos.row) << " " << int(pos.col) << "\n";
#endif // VISUALISER
	state.coverageMap[pos.row][pos.col]++;

	// Found new ground
	if (state.coverageMap[pos.row][pos.col] == 1 && state.waterMap[pos.row][pos.col] == MAP_UNKNOWN) {
		state.waterMap[pos.row][pos.col] = MAP_GROUND;
	}
}

void inline coverageReducePos(Location pos) {
#ifdef VISUALISER
	std::cout << "v setFillColor 255 0 0 .5\n";
	std::cout << "v tile " << int(pos.row) << " " << int(pos.col) << "\n";
#endif // VISUALISER
	state.coverageMap[pos.row][pos.col]--;
	assert(state.coverageMap[pos.row][pos.col] >= 0);
}

void coverageAddAnt(Ant ant) {
	std::vector<Location> circle = ants::getCircleFromCache(ant, state.viewRadiusCache, ants::CIRCLE_FILLED);
	for (int i = 0; i < circle.size();i++)
		coverageRaisePos(circle[i]);
}

void coverageUpdateAnt(Ant ant) {
	std::vector<Location> circle = ants::getCircleFromCache(ant, state.viewRadiusCache, ants::CIRCLE_HORIZONTAL);
	switch(ant.direction) {
	case NORTH:
		for (int i = circle.size();i--;) {
			coverageRaisePos (Location(int(-circle[i].col - 1), int(circle[i].row)));
			coverageReducePos(Location(int( circle[i].col    ), int(circle[i].row)));
		}
		return;
	case SOUTH:
		for (int i = circle.size();i--;) {
			coverageRaisePos (Location(int( circle[i].col + 1), int(circle[i].row)));
			coverageReducePos(Location(int(-circle[i].col    ), int(circle[i].row)));
		}
		return;
	case EAST:
		for (int i = circle.size();i--;) {
			coverageRaisePos (Location(int(-circle[i].row - 1), int(circle[i].col)));
			coverageReducePos(Location(int( circle[i].row    ), int(circle[i].col)));
		}
		return;
	case WEST:
		for (int i = circle.size();i--;) {
			coverageRaisePos (Location(int( circle[i].row + 1), int(circle[i].col)));
			coverageReducePos(Location(int(-circle[i].row    ), int(circle[i].col)));
		}
		return;
	}
}

void coverageRemoveAnt(Ant ant) {
	std::vector<Location> circle = ants::getCircleFromCache(ant, state.viewRadiusCache, ants::CIRCLE_FILLED);
	for (int i = 0; i < circle.size();i++)
		coverageReducePos(circle[i]);
}

// Enemy tracker
void inline trackEnemy(Ant ant) {
	std::vector<Location> circle = ants::getCircleFromCache(ant, state.viewRadiusCache, ants::CIRCLE_FILLED);
	for (int i = 0;i < circle.size();i++)
		state.enemyCoverageMap[ant.owner][circle[i].row][circle[i].col] = true;
}

// Water
void inline updateWater(unsigned char row, unsigned char col) {

	Location location(row, col);

	if (MAP_WATER != location.mapType()) {
		location.mapType(MAP_WATER);

		Location northNorth, eastEast, southSouth, westWest,
			northWest, southEast, northEast, southWest, tmp;

		northNorth = location.move(-2,  0);
		eastEast   = location.move( 0,  2);
		southSouth = location.move( 2,  0);
		westWest   = location.move( 0, -2);

		northWest  = location.move(-1, -1);
		southEast  = location.move( 1,  1);
		northEast  = location.move(-1,  1);
		southWest  = location.move( 1, -1);

		// Find land surrounded with 3 tiles of water
		if (MAP_WATER == southWest.mapType()) {
			tmp = location.move(WEST);
			if (MAP_WATER != tmp.mapType()) {
				if      (MAP_WATER == northWest.mapType())  tmp.mapType(MAP_WATER);
				else if (MAP_WATER == westWest.mapType())   tmp.mapType(MAP_WATER);
			}
			tmp = location.move(SOUTH);
			if (MAP_WATER != tmp.mapType()) {
				if      (MAP_WATER == southEast.mapType())  tmp.mapType(MAP_WATER);
				else if (MAP_WATER == southSouth.mapType()) tmp.mapType(MAP_WATER);
			}
		}
		if (MAP_WATER == northEast.mapType()) {
			tmp = location.move(EAST);
			if (MAP_WATER != tmp.mapType()) {
				if      (MAP_WATER == southEast.mapType())  tmp.mapType(MAP_WATER);
				else if (MAP_WATER == eastEast.mapType())   tmp.mapType(MAP_WATER);
			}
			tmp = location.move(NORTH);
			if (MAP_WATER != tmp.mapType()) {
				if      (MAP_WATER == northWest.mapType())  tmp.mapType(MAP_WATER);
				else if (MAP_WATER == northNorth.mapType()) tmp.mapType(MAP_WATER);
			}
		}
		if (MAP_WATER == southEast.mapType()) {
			tmp = location.move(EAST);
			if (MAP_WATER == eastEast.mapType())   tmp.mapType(MAP_WATER);
			tmp = location.move(SOUTH);
			if (MAP_WATER == southSouth.mapType()) tmp.mapType(MAP_WATER);
		}
		if (MAP_WATER == northWest.mapType()) {
			tmp = location.move(WEST);
			if (MAP_WATER == westWest.mapType())   tmp.mapType(MAP_WATER);
			tmp = location.move(NORTH);
			if (MAP_WATER == northNorth.mapType()) tmp.mapType(MAP_WATER);
		}
	}
}

// Food
void inline removeFood(int row, int col) {
	// Get pointer
	Object object(row, col);
	Object *p = state.foodMap[object.row][object.col];
	assert(p);

	// Add to gathered food list
	state.gatheredFoodList.push_back(object);

#ifdef VISUALISER
	std::cout << "v setFillColor 255 128 0 .5\n";
	std::cout << "v tile " << int(object.row) << " " << int(object.col) << "\n";
#endif // VISUALISER

	// Remove from grid
	state.foodMap[p->row][p->col] = 0;

	// Remove from visible food
	for (std::vector<Object *>::iterator it = state.visibleFoodList.begin(); it != state.visibleFoodList.end(); it++) {
		if (p == *it) {
			state.visibleFoodList.erase(it);
			goto skipHiddenFood;
		}
	}

	// Remove from hidden food if not found in visible food
	for (std::deque<Object *>::iterator it = state.hiddenFoodList.begin(); it != state.hiddenFoodList.end(); it++) {
		if (p == *it) {
			state.hiddenFoodList.erase(it);
			goto skipHiddenFood;
		}
	}

	assert(false);

skipHiddenFood:
	// Remove from general food list
	for (std::deque<Object>::iterator it = state.foodList.begin(); it != state.foodList.end(); it++) {
		if (p == &*it) {
			state.foodList.erase(it);
			return;
		}
	}

	assert(false);
}

void inline updateFood(int row, int col) {
	Object object(row, col);
	Object *p;

	// Confirm we have this piece of food
	if (state.foodMap[object.row][object.col]) {
		// Food should be located in lost food list
		p = 0;
		for (std::deque<Object *>::iterator it = state.hiddenFoodList.begin(); it != state.hiddenFoodList.end(); it++) {
			if ((*it)->row == row && (*it)->col == col) {
				p = *it;
				state.hiddenFoodList.erase(it);
				break;
			}
		}
		assert(p);

		// Move food into visible food list
		state.visibleFoodList.push_back(p);
	} else {
		// Add food in grid, food list and visible food list
		state.foodList.push_back(object);
		p = &state.foodList.back();

		state.foodMap[object.row][object.col] = p;
		state.visibleFoodList.push_back(p);
		state.newFoodList.push_back(p);

#ifdef VISUALISER
		std::cout << "v setFillColor 255 255 0 .5\n";
		std::cout << "v tile " << int(object.row) << " " << int(object.col) << "\n";
#endif // VISUALISER
	}
}

void inline prepareFood() {
	std::vector<Object *> trash;

	// find out the cause why this piece of food was missing
	for (std::deque<Object *>::iterator it = state.hiddenFoodList.begin(); it != state.hiddenFoodList.end(); it++) {
		if (state.coverageMap[(*it)->row][(*it)->col] == 0) continue; // fog of war

		// eaten by fog of war
		if (state.coverageMap[(*it)->row][(*it)->col] == 0) continue;

		// eaten by ant
		int owner = -1;
		std::vector<Location> locations = ants::getCircleFromCache(**it, state.spawnRadiusCache, ants::CIRCLE_EDGE);
		for (std::vector<Location>::iterator locIt = locations.begin(); locIt != locations.end(); locIt++) {
			if (state.antsMap[int(locIt->row)][int(locIt->col)]) {
				if (owner == -1) {
					trash.push_back(*it);
					owner = int(state.antsMap[locIt->row][locIt->col]->owner);
				}
				else if (owner != state.antsMap[locIt->row][locIt->col]->owner) {
					// at least ants of 2 different owners means is food gone
					owner = -2;
					break;
				}
			}
		}
		if (owner == 0) state.hive++;
		if (owner != -1) continue;

		// located at the edge of fog of war (TODO combine with coverage map to get more details)
		for (std::vector<Location>::iterator it = locations.begin(); it != locations.end(); it++) {
			if (state.coverageMap[it->row][it->col] == 0) {
				owner = -2;
			}
		}
		if (owner != -1) continue;

		assert(false);
	}

	// remove food we think are gone
	for (std::vector<Object *>::iterator it = trash.begin(); it != trash.end(); it++)
		removeFood((*it)->row, (*it)->col);
}
void inline cleanupFood() {
	// Mark all food as hidden (put food in front to reduce iterations)
	for (std::vector<Object *>::iterator it = state.visibleFoodList.begin(); it != state.visibleFoodList.end(); it++) {
		state.hiddenFoodList.push_front(*it);
	}
	state.visibleFoodList.clear();
	state.newFoodList.clear();
	state.gatheredFoodList.clear();
}

// Ants
void foundLostAnt(unsigned char row, unsigned char col) {
	Ant *ant = state.antsMap[row][col];

	assert(ant->owner == 0);

	// Remove ant from global dead ants list
	for (std::vector<Ant *>::iterator it = state.deadList.begin(); it != state.deadList.end(); it++) {
		if (ant == *it) {
			state.deadList.erase(it);
			break;
		}
	}

	// Remove ant from friendly dead ants list
	for (std::vector<Ant *>::iterator it = state.myDeadAntsList.begin(); it != state.myDeadAntsList.end(); it++) {
		if (ant == *it) {
			state.myDeadAntsList.erase(it);
			return;
		}
	}

	assert(false);
}

void removeAnt(Ant *ant) {
	// Remove ant from map
	state.antsMap[ant->row][ant->col] = 0;

#ifdef VISUALISER
		std::cout << "v setLineColor 0 255 0 .8\n";
		std::cout << "v circle " << int(ant->row) << " " << int(ant->col) << " 1 false\n";
#endif // VISUALISER

	// Remove ant from friendly or enemy ants list
	if (ant->owner == 0) {
		// Remove coverage
		coverageRemoveAnt(*ant);

		for (std::deque<Ant *>::iterator it = state.myAntsList.begin(); it != state.myAntsList.end(); it++) {
			if (ant == *it) {
				state.myAntsList.erase(it);
				break;
			}
		}
	} else {
		for (std::deque<Ant *>::iterator it= state.enemyAntsList.begin(); it != state.enemyAntsList.end(); it++) {
			if (ant == *it) {
				state.enemyAntsList.erase(it);
				break;
			}
		}
	}

	// Remove ant from global list
	for (std::deque<Ant>::iterator it = state.antsList.begin(); it != state.antsList.end(); it++) {
		if (ant == &*it) {
			state.antsList.erase(it);
			return;
		}
	}

	assert(false);
}

void inline cleanupAnts() {
	std::vector<Ant *> deleteAnts;

	// Move ants in desired direction
	for (std::vector<Ant *>::iterator it = state.movedAntsList.begin(); it != state.movedAntsList.end(); it++) {
		// update pos
		(*it)->row = (*it)->nextLoc.row;
		(*it)->col = (*it)->nextLoc.col;

		// update coverage map
		coverageUpdateAnt(**it);
	}

	// Clean up lists
	state.deadList.clear();
	state.myDeadAntsList.clear();
	state.enemyDeadAntsList.clear();
	state.movedAntsList.clear();

	// Mark all ants as potentionally been lost
	for (std::deque<Ant>::iterator it = state.antsList.begin(); it != state.antsList.end(); it++) {
		if (it->owner != 0) {
			deleteAnts.push_back(&(*it));
			continue;
		}

		// Add everybody to the deadlists
		state.myDeadAntsList.push_back(&(*it));
	}

	// delete enemies
	for (std::vector<Ant *>::iterator it = deleteAnts.begin(); it != deleteAnts.end(); it++) {
		removeAnt(*it);
	}
}

void inline prepareAnts() {
	// Notify and clean up dead ants
	for (std::vector<Ant *>::iterator it = state.myDeadAntsList.begin(); it != state.myDeadAntsList.end(); it++) {
		if (!(*it)->isDead) {
			logger << " - Error: lost ant at (" << int((*it)->row) << ", " << int((*it)->col) << ")\n";
		}

		removeAnt(&(**it));
	}
}

void inline updateAnts(int row, int col, int owner) {
	Ant ant(row, col, owner);
	Ant *p;

	// Try to match with existing ants and put the ant in the right lists/maps
	if (owner == 0) {
		// Mark ant as found
		if (state.antsMap[ant.row][ant.col]) return foundLostAnt(row, col);

		state.antsList.push_back(ant);
		p = &state.antsList.back();

		// Friendly list
		state.myAntsList.push_back(p);

		// Coverage
		coverageAddAnt(*p);

		// Reduce hive count if necessary
		if (state.round != 1) {
			assert(state.hive > 0);
			state.hive--;
		}
	} else {
		state.antsList.push_back(ant);
		p = &state.antsList.back();

		// Enemy list
		state.enemyAntsList.push_back(p);

		// Tracker
		trackEnemy(*p);
	}

	// Map
	state.antsMap[ant.row][ant.col] = p;

#ifdef VISUALISER
		std::cout << "v setLineColor 0 255 0 .8\n";
		std::cout << "v circle " << int(ant.row) << " " << int(ant.col) << " 1 false\n";
#endif // VISUALISER
}

void inline notifyDeadAnt(int row, int col, unsigned char owner) {
	Ant ant(row, col, owner);

	// trigger event

	// Check if we already know this dead object
	if (!state.antsMap[ant.row][ant.col]) {
		if (owner == 0) logger << " - Error: missing friendly dead ant object at " << int(row) << ", " << int(col) << "\n";
		else trackEnemy(ant); // We still have to track enemies
	} else {
		ant = *state.antsMap[ant.row][ant.col];
		if (ant.owner == 0) {
			ant.isDead = true;
		}
	}
}

// Hills
void inline updateHills(int row, int col, unsigned char owner) {
	Object object(row, col, owner);

	//Skip if we knew this already
	for (std::deque<Object>::iterator it = state.hillList.begin(); it != state.hillList.end(); it++) {
		if (it->row == row && it->col == col) return;
	}
}

void inline prepareHills() {
	
}

void inline cleanupHills() {
	
}

// Stream

void StreamStartUp() {
	std::string input;
	std::stringstream ss;
	unsigned int tmp;

#ifdef DEBUG
	state.startTime.start();
	logger.open("debug.txt");
#endif // DEBUG

	std::cout.sync_with_stdio(0); // Should make our bot faster

	std::getline(std::cin, input);
	if (input != "turn 0") exit(-1);

#ifdef DEBUG
	state.roundTime.start();
	logger << "== Bootstrap (" << state.startTime.getTime() << " ms) ==\n";
#endif // DEBUG

	while (std::getline(std::cin, input)) {
		ss.clear();
		ss << input;
		ss >> input;

		if      (input == "ready")         break;
		else if (input == "rows")         {ss >> tmp; state.rows    = tmp;}
		else if (input == "cols")         {ss >> tmp; state.cols    = tmp;}
		else if (input == "players")      {ss >> tmp; state.resizePlayers(int(tmp));}
		else if (input == "turns")         ss >> state.turns;
		else if (input == "viewradius2")   ss >> state.viewRadius2;
		else if (input == "attackradius2") ss >> state.attackRadius2;
		else if (input == "spawnradius2")  ss >> state.spawnRadius2;
		else if (input == "loadtime")      ss >> state.loadTime;
		else if (input == "turntime")      ss >> state.turnTime;
		else if (input == "player_seed")   ss >> state.playerSeed;
		else {
			getline(ss, input);
			logger << "Unknow " << input << "\n";
		}
	}

	state.init();
}

void StreamPrepared() {
#ifdef DEBUG
	logger << " - Lap time: " << state.roundTime.getTime() << " ms (" << state.startTime.getTime() << " ms)\n";
#endif // DEBUG

	std::cout << "go\n";
	std::cout.flush();
}

bool StreamNextRound() {
	unsigned int r, c, o;
	std::string input;
	std::stringstream ss;

	ss << ++state.round;
	getline(std::cin, input);
	if (input == "end") return false;
	if (input != "turn " + ss.str()) exit(-1);

#ifdef DEBUG
	state.roundTime.start();
	logger << "\n== Turn " << state.round << " (" << state.startTime.getTime() << " ms) ==\n";
#endif // DEBUG

	while(std::getline(std::cin, input)) {
		if (input[1] == ' ') {
			ss.clear();
			ss << input;
			     if (input[0] == 'w') {ss >> input; ss >> r >> c; updateWater(r, c);}
			else if (input[0] == 'f') {ss >> input; ss >> r >> c; updateFood (r, c);}
			else if (input[0] == 'a') {ss >> input; ss >> r >> c >> o; state.resizePlayers(o); updateAnts   (r, c, o);}
			else if (input[0] == 'd') {ss >> input; ss >> r >> c >> o; state.resizePlayers(o); notifyDeadAnt(r, c, o);}
			else if (input[0] == 'h') {ss >> input; ss >> r >> c >> o; state.resizePlayers(o); updateHills  (r, c, o);}
		} else if (input == "go") {
			break;
		} else {
			getline(ss, input);
			logger << "Unknow " << input << "\n";
		}
	}

	// prepare event
	prepareAnts();
	prepareFood();
	prepareHills();

	logger << " - Objects: ";
	logger <<    "ants: "      << state.antsList.size() << " (" << state.myAntsList.size()      << " : " << state.enemyAntsList.size()     << ")";
	logger << " - dead ants: " << state.deadList.size() << " (" << state.myDeadAntsList.size()  << " : " << state.enemyDeadAntsList.size() << ")";
	logger << " - food: "      << state.foodList.size() << " (" << state.visibleFoodList.size() << " : " << state.hiddenFoodList.size();
	logger <<                                            " | +" << state.newFoodList.size()     << " -"  << state.gatheredFoodList.size() << ")";
	logger << " - hive: "      << int(state.hive);
	// hills
	// food
#ifdef DEBUG
	logger << " - (" << state.roundTime.getTime() << " ms)";
#endif //DEBUG
	logger << "\n";

	return true;
}

void StreamFlush() {
	// output buffer
	for (std::vector<Ant *>::iterator it = state.movedAntsList.begin(); it != state.movedAntsList.end(); it++)
		std::cout << "o " << int((*it)->row) << " " << int((*it)->col) << " " << (*it)->direction << "\n";

	// cleanup event
	cleanupAnts();
	cleanupFood();
	cleanupHills();

#ifdef DEBUG
	logger << " - Lap time: " << state.roundTime.getTime() << " ms (" << state.startTime.getTime() << " ms)\n";
#endif // DEBUG

	std::cout << "go\n";
}

// Public api

namespace ants {
	void inline Event::onConfig(){}
	void inline Event::onRound(){}
	void inline Event::onShutDown(){}

	std::vector<Event> emitter;

	int run(std::vector<Event> events) {
#ifdef CRASH_HANDLER
		signal(SIGSEGV, ants::crashHandler);
		signal(SIGABRT, ants::crashHandler);
#endif // CRASH_HANDLER

		emitter = events;

		StreamStartUp();
		for (int i = 0; i < events.size(); i++) events[i].onConfig();

		StreamPrepared();
		while (StreamNextRound()) {
			for (int i = 0; i < events.size(); i++) events[i].onRound();
			StreamFlush();
		}

		for (int i = 0; i < events.size(); i++) events[i].onShutDown();

		logger << "\n *** end game ***\n";

		return 0;
	}
}
