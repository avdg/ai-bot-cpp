#ifndef LOGGER_H_
#define LOGGER_H_

#include <fstream>
#include <iostream>

struct Logger {
	std::ofstream file;

	Logger(){};
	~Logger(){close();}

	inline void open(const std::string &filename);
	inline void close();
};

inline Logger& operator<<(Logger &log, std::ostream& (*manipulator)(std::ostream&));

template <class T>
inline Logger& operator<<(Logger &log, const T &t);

// Implementation of definitions
inline void Logger::open(const std::string &filename) {
	#ifdef DEBUG
		file.open(filename.c_str());
	#endif // DEBUG
}

inline void Logger::close() {
	#ifdef DEBUG
		file.close();
	#endif // DEBUG
}

inline Logger& operator<<(Logger &log, std::ostream& (*manipulator)(std::ostream&)) {
	#ifdef DEBUG
		log.file << manipulator;
	#endif // DEBUG

	#ifdef CERR
		std::cerr << manipulator;
	#endif // CERR

	return log;
}

template <class T>
inline Logger& operator<<(Logger &log, const T &t) {
	#ifdef DEBUG
		log.file << t;
	#endif // DEBUG

	#ifdef CERR
		std::cerr << t;
	#endif // CERR

	return log;
};

#endif //LOGGER_H_
