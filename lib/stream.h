#ifndef LIB_STREAM_
#define LIB_STREAM_

#include "state.h"

extern State state;

namespace ants {
	struct Event {
		void inline onConfig();
		void inline onRound();
		void inline onShutDown();
	};

	int run(std::vector<Event> events);
}

#endif // LIB_STREAM_
