#ifndef LIB_STATE_
#define LIB_STATE_

#include <algorithm>
#include <bitset>
#include <cmath>
#include <deque>
#include <vector>

#ifdef CRASH_HANDLER
	#include "crash.h"
#endif // CRASH_HANDLER
#ifdef DEBUG
	#include "timer.h"
#endif // DEBUG

const int  TDIRECTIONS      = 4;
const char CDIRECTIONS[4]   = {'N', 'E', 'S', 'W'};
const int  DIRECTIONS[4][2] = { {-1, 0}, {0, 1}, {1, 0}, {0, -1} };
enum       Direction          { NORTH, EAST, SOUTH, WEST };
enum       MapType            { MAP_UNKNOWN, MAP_WATER, MAP_GROUND, MAP_SURROUNDED };

struct Relative {
	short row, col;

	inline Relative(){row = col = 0;};
	Relative(int r, int c);
};

struct Location {
	unsigned char row, col;

	inline Location() {row = col = 0;};
	inline Location(unsigned char r, unsigned char c) {row = r; col = c;};
	Location(int r, int c);

	MapType mapType();
	void mapType(MapType);

	float distance(Location location);
	float distance(int r, int c);

	short manhatten(Location location);
	short manhatten(int r, int c);

	Location move(Direction direction);
	Location move(Relative relative);
	Location move(int r, int c);
};

struct Object : Location {
	unsigned char owner;
	bool visible;

	inline Object():Location(){owner = 0;visible=true;};
	inline Object(unsigned char o):Location(){owner = o;visible=true;};

	inline Object(int r, int c):Location(r,c){owner=0;visible=true;};
	inline Object(int r, int c, unsigned char o):Location(r,c){owner=o;visible=true;};

	inline Object(unsigned char r, unsigned char c):Location(r, c){owner=0;visible=true;};
	inline Object(unsigned char r, unsigned char c, unsigned char o):Location(r, c){owner=o;visible=true;};
};

struct Ant : Object {
	Direction direction; // Last set direction
	Location nextLoc;

	bool isDead;

	inline Ant():Object(){};
	inline Ant(unsigned char o):Object(o){constructorHelper();};

	inline Ant(int r, int c):Object(r,c){constructorHelper();};
	inline Ant(int r, int c, unsigned char o):Object(r,c, o){constructorHelper();};

	inline Ant(unsigned char r, unsigned char c):Object(r, c){constructorHelper();};
	inline Ant(unsigned char r, unsigned char c, unsigned char o):Object(r, c, o){constructorHelper();};

private:
	inline void constructorHelper();
};

struct State {
public:
	// Config
	// Note: players are only given at start of visualizer data (not in real games)
	unsigned char  rows, cols, players;
	unsigned short turns, viewRadius2, attackRadius2, spawnRadius2;
	unsigned long  loadTime, turnTime;
	long long      playerSeed;

	// Basic variables
	unsigned short int hive;
	unsigned short int round;
	unsigned short int totalMyHills; // Can be used to estimate number of enemy hills

#ifdef DEBUG
	Timer startTime, roundTime;
#endif // DEBUG

	// Memoize
	std::vector<Relative> attackRadiusCache, viewRadiusCache, spawnRadiusCache;

	// Maps
	std::vector<std::vector<MapType> >  waterMap;
	std::vector<std::vector<Object *> > foodMap;
	std::vector<std::vector<Ant *> >    antsMap;
	std::vector<std::vector<short> >    coverageMap;

	std::vector<std::bitset<200> >               safetyMap; // tracks possible enemylocations
	std::vector<std::vector<std::bitset<200> > > enemyCoverageMap;

	// Lists
	std::deque<Object> foodList;
	std::vector<Object *> visibleFoodList;
	std::deque<Object *> hiddenFoodList;
	std::vector<Object *> newFoodList;
	std::vector<Object> gatheredFoodList;

	std::deque<Object> hillList;
	std::deque<Object *> myHillsList;
	std::deque<Object *> enemyHillsList;
	std::deque<Object *> hiddenHillsList;

	std::deque<Ant> antsList;
	std::deque<Ant *> myAntsList;
	std::deque<Ant *> enemyAntsList;
	std::vector<Ant *> movedAntsList;

	std::vector<Ant *> deadList;
	std::vector<Ant *> myDeadAntsList;
	std::vector<Ant *> enemyDeadAntsList;

	State() {
		players = 1;
		round   = totalMyHills = 0;
	};

	void init();
	void resizePlayers(unsigned char players);
};

extern State state;

// Global functions
namespace ants {
	enum CircleType { CIRCLE_VERTICAL, CIRCLE_HORIZONTAL, CIRCLE_FILLED, CIRCLE_EDGE };

	// Circle
	       std::vector<Relative> getCircleCache(int r2);
	       std::vector<Location> getCircleFromCache(Location pos, std::vector<Relative> cache, CircleType type);
	inline std::vector<Location> getCircle(Location pos, int r2, CircleType type){
		return getCircleFromCache(pos, getCircleCache(r2), type);
	}
;
}

// Inlined (simple) methods (dependent of the existence of state or for clarity)

// Relative location object
inline Relative::Relative(int r, int c) {
	row = r % state.rows;
	col = c % state.cols;
}

// Location object
inline Location::Location(int r, int c) {
	row = r % state.rows;
	col = c % state.cols;
}

inline MapType Location::mapType()                { return state.waterMap[row][col];}
inline void    Location::mapType(MapType mapType) { state.waterMap[row][col] = mapType;}

inline float Location::distance(Location location) {
	int r, c;

	r = std::min(int(location.row), state.rows - location.row);
	c = std::min(int(location.col), state.cols - location.col);

	return std::sqrt(r*r + c*c);
}
inline float Location::distance(int r, int c) { return this->distance(Location(r, c));}

inline short Location::manhatten(Location location) {
	return
		std::min(int(location.row), state.rows - location.row) +
		std::min(int(location.col), state.cols - location.row);
}
inline short Location::manhatten(int r, int c) {return this->distance(Location(r, c));}

inline Location Location::move(Direction direction) {
	return Location (
		(row + state.rows + DIRECTIONS[direction][0]) % state.rows,
		(col + state.cols + DIRECTIONS[direction][1]) % state.cols
	);
}
inline Location Location::move(Relative relative) {
	return Location (
		(row + state.rows + relative.row) % state.rows,
		(col + state.cols + relative.col) % state.cols
	);
}
inline Location Location::move(int r, int c) {
	return Location (
		(row + state.rows + r) % state.rows,
		(col + state.cols + c) % state.cols
	);
}

// Ant
inline void Ant::constructorHelper() {
	// direction is random

	nextLoc.row = row;
	nextLoc.col = col;
	isDead = false;
}

// State
inline void State::init() {
	int i, j;

	// Water map
	std::vector<MapType> tmpVector1;
	tmpVector1.resize(cols, MAP_UNKNOWN);
	waterMap.resize(rows, tmpVector1);

	// Ants map
	std::vector<Ant *> tmpVector2;
	tmpVector2.resize(cols, 0);
	antsMap.resize(rows, tmpVector2);

	// Food map
	std::vector<Object *> tmpVector3;
	tmpVector3.resize(cols, 0);
	foodMap.resize(rows, tmpVector3);

	// Coverage map (own ants)
	std::vector<short> tmpVector4;
	tmpVector4.resize(cols, 0);
	coverageMap.resize(rows, tmpVector4);

	// enemyTracker
	std::bitset<200> tmpBitset;
	tmpBitset.set();
	safetyMap.resize(rows, tmpBitset);

	// Precalculated circles
	viewRadiusCache   = ants::getCircleCache(viewRadius2);
	attackRadiusCache = ants::getCircleCache(attackRadius2);
	spawnRadiusCache  = ants::getCircleCache(spawnRadius2);

	totalMyHills = 0;
	hive = 0;
}

void inline State::resizePlayers(unsigned char p) {
	if (p < players) return;

	players = p + 1;

	std::bitset<200>               tmpBitset;
	std::vector<std::bitset<200> > tmpVector;

	tmpBitset.reset();
	tmpVector.resize(rows, tmpBitset);
	enemyCoverageMap.resize(players, tmpVector);
}

#endif // LIB_STATE_
