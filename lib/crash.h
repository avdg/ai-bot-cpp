#ifndef LIB_CRASH_
#define LIB_CRASH_

#include <execinfo.h>
#include <signal.h>

namespace ants {
	inline void crashHandler(int sig) {
		void *array[10];
		size_t size;

		size = backtrace(array, 50);

		fprintf(stderr, "Error: signal %d:\n", sig);
		backtrace_symbols_fd(array, size, 2);
		exit(1);
	}
}

#endif // LIB_CRASH_
