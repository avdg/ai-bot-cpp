#include <cassert>
#include <cmath>

#include "state.h"

State state;

namespace ants {
	// Calculates circle values enough to create a half circle
	// TODO: use emplace_back instead of push_back if compiler supports it
	std::vector<Relative> getCircleCache(int r2) {
		std::vector<Relative> cache;
		std::vector<Relative> tmp; // Values needed to complete the half circle

		assert(r2 >= 0);
		if (r2 < 1) return cache;

		// Base iteration (> 50% of half circle + cached values for later)
		int j = std::ceil(std::sqrt(r2));
		for (int iTemp, jTemp, i = 0;i <= j;i++) {
			iTemp = i*i;
			jTemp = j;
			while((iTemp + j*j) > r2) j--; //Find biggest value in current row, starting from edge/outside
			cache.push_back(Relative(i, j)); // Add values
			if (i != 0) cache.push_back(Relative(-i, j)); // Opposite side
			if (jTemp != j && i > 0) tmp.push_back(Relative(i - 1, jTemp)); // Values to complete half circle
		}

		assert(cache.size() > 2);
		assert(tmp.size()   > 0);

		// Erase duplicates
		if (
			abs(cache[cache.size() - 2].row) == abs(cache[cache.size() - 3].col) &&
			abs(cache[cache.size() - 2].col) == abs(cache[cache.size() - 3].row)
		) cache.resize(cache.size() - 2);

		// Complete half circle
		for (int i = tmp.size(); i--;) {
			cache.push_back(Relative(tmp[i].col, tmp[i].row));
			cache.push_back(Relative(int(-tmp[i].col), int(tmp[i].row)));
		}

		// Do not forget the top and bottom edges of the half circle if necessary
		if (tmp[0].col != cache[0].col) {
			cache.push_back(Relative( cache[0].col, 0));
			cache.push_back(Relative(-cache[0].col, 0));
		}

		return cache;
	}

	// Creates a circle from a given cache
	// TODO: use emplace_back instead of push_back if compiler supports it
	std::vector<Location> getCircleFromCache(Location pos, std::vector<Relative> cache, CircleType type) {
		std::vector<Location> circle;

		switch (type) {
		case CIRCLE_VERTICAL:
			for (int i = 0; i < cache.size();i++)
				circle.push_back(pos.move(cache[i].col, cache[i].row));
			return circle;
		case CIRCLE_HORIZONTAL:
			for (int i = 0; i < cache.size();i++)
				circle.push_back(pos.move(cache[i]));
			return circle;
		case CIRCLE_FILLED:
			for (int i = 0; i < cache.size();i++)
				for (int j = cache[i].col, l = -j; j >= l; j--)
					circle.push_back(pos.move(cache[i].row, j));
			return circle;
		case CIRCLE_EDGE:
		default:
			int i = 0;
			for (;i < cache.size() && cache[i].col > cache[i].row;i++) {
				circle.push_back(pos.move( cache[i].col,  cache[i].row));
				circle.push_back(pos.move(-cache[i].col,  cache[i].row));
				circle.push_back(pos.move( cache[i].row,  cache[i].col));
				circle.push_back(pos.move( cache[i].row, -cache[i].col));
			}
			// Don't handle this in the loop as i-1 will create duplicates
			if (i > 0 && cache[i].row == cache[i].col) {
				circle.push_back(pos.move( cache[i].col,  cache[i].col));
				circle.push_back(pos.move( cache[i].col, -cache[i].col));
				circle.push_back(pos.move(-cache[i].col,  cache[i].col));
				circle.push_back(pos.move(-cache[i].col, -cache[i].col));
			}
			return circle;
		}
	}
}
